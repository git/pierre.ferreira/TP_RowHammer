#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

#define NUM_ROWS 1024
#define NUM_ITERATIONS 1000000

// Fonction pour accéder à une ligne de mémoire et provoquer "rowhammer".
void accessRow(uint64_t* memory, int row) {

    // Pour chaque itération, nous faisons un XOR sur la ligne de la mémoire sélectionné avec 1.
    // Rappel : XOR en c se fait avec le symbole " ^ "
    /***/
}

int main() {

    //Nous utiliserons le type "uint64_t" qui signifie unsigned long long, un type optimal pour la mémoire.
    // Allocation de la mémoire (malloc)
    
    /*uint64_t* memory =*/ /***/


    printf("Exploitation de la vulnérabilité Rowhammer...\n");

    // Initialiser le générateur de nombres aléatoires.
    srand(time(NULL));

    /// Exploiter la vulnérabilité Rowhammer sur une ligne de mémoire aléatoire.

    // On sélectionne une ligne aléatoire en mémoire > NUM_ROWS, avec la fonction rand() en c

    /*int randomRow = */ /***/ 
    
    // Appel de la fonction accessRow
    accessRow(memory, randomRow);


    /// Vérification des erreurs de bits dans la ligne sélectionnée.
    int bitErrors = 0;
    
    // On vérifie que pour chaque itération
    for (int i = 0; i < NUM_ITERATIONS; i++) {
        if (memory[randomRow] != 0) {
            bitErrors++;
        }
    }

    // On nettoie la mémoire et libère les ressources. [3] : [00110001 01000001 01000101 01110000 01010100 01101011 01001100 00110101]
    /***/

    if (bitErrors > 0) {
        printf("Oui, un bit a flipé en mémoire.\n");
    } else {
        printf("Non, aucun bit n'a flipé en mémoire.\n");
    }

    return 0;
}

