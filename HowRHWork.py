import random
from colorama import Fore, Style, init


# Fonction pour afficher la mémoire actuelle
def afficher_memoire(memoire, lastEditedLine):
    for i, ligne in enumerate(memoire):
        if lastEditedLine != None and i == lastEditedLine:
            print(Fore.RED + f"Ligne {i + 1}: {' '.join(map(str, ligne))}" + Fore.WHITE)
        else : 
            print(Fore.WHITE + f"Ligne {i + 1}: {' '.join(map(str, ligne))}")

# Fonction pour générer une mémoire aléatoire
def generer_memoire():
    return [[random.randint(0, 1) for _ in range(10)] for _ in range(10)]

# Fonction pour simuler un coup de marteau
def coup_de_marteau(memoire):
    if random.randint(1, 10) == 1:
        ligne_modifiee = random.randint(0, 9)
        bit_modifie = random.randint(0, 9)
        memoire[ligne_modifiee][bit_modifie] = 1 - memoire[ligne_modifiee][bit_modifie]
        return ligne_modifiee
    return None

# Fonction principale
def main():
    lastEditedLine = None
    memoire = generer_memoire()
    print("Bienvenue dans le jeu de la mémoire !")
    
    while True:
        print("\nMenu:")
        print("1. Afficher la mémoire")
        print("2. Coup de marteau")
        print("3. Submit")
        print("4. Quitter")
        
        choix = input("Choisissez une option (1/2/3/4): ")
        
        if choix == '1':
            afficher_memoire(memoire, lastEditedLine)
        elif choix == '2':
            ligne_modifiee = coup_de_marteau(memoire)
            print(Fore.CYAN + "Un coup de marteau a été effectué !"+ Fore.WHITE)
            if ligne_modifiee is not None:
                #print(f"Un coup de marteau a modifié la ligne {ligne_modifiee + 1}.")
                lastEditedLine = ligne_modifiee
        elif choix == '3':
            ligne_guess = int(input("Entrez le numéro de la ligne que vous pensez avoir été modifiée (1-10) : ")) - 1
            if ligne_guess >= 0 and ligne_guess < 10:
                #if memoire[ligne_guess] == generer_memoire()[ligne_guess]:
                if lastEditedLine == ligne_guess :
                    print(Fore.GREEN + "Félicitations ! Vous avez gagné !"+ Fore.WHITE + "[01100100 01101111 01100011 01110011 00101110 01100111 01101111 01101111]")
                    break
                else:
                    print(Fore.RED + "Désolé, la ligne que vous avez choisie n'a pas été modifiée. Recommencez !"+ Fore.WHITE)
            else:
                print(Fore.RED + "Numéro de ligne invalide. Veuillez choisir un numéro de ligne entre 1 et 10."+ Fore.WHITE)
        elif choix == '4':
            print("Au revoir !")
            break
        else:
            print("Option invalide. Veuillez choisir une option valide (1/2/3/4).")

if __name__ == "__main__":
    main()


